#ifndef EUCLID_H
#define EUCLID_H

void extended_euclid(int a, int b, int *x)
/* calculates a * x + b *y = gcd(a, b)  */
{
  int q, r, x1, x2;
  if (b == 0) {
    *x = 1;
    return;
  }

  x2 = 1, x1 = 0;

  while (b > 0) {
    q = a / b, r = a - q * b;
    *x = x2 - q * x1;
    a = b, b = r;
    x2 = x1, x1 = *x;
  }
  *x = x2;
}

#endif // EUCLID_H
