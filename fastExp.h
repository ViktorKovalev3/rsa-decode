#ifndef FASTEXP_H
#define FASTEXP_H
double fastExp(int &x, int y, int &z)
{
    int result = 1;
    while (y != 0)
    {
        if (y % 2 != 0)
        {
            result = result * x % z;
            y -= 1;
        }
        x = x*x % z;
        y /= 2;
    }
    return result;
}
#endif // FASTEXP_H
