#include <iostream>         //cin, cout
#include <string>           //string
#include <locale>           //Русский язык
#include "phi.h"            //Вычисление phi
#include "extendedEuclid.h" //Вычисление бинома
#include "fastExp.h"        //Возводим в степень
#include "modFunction.h"    //Решение пролемы с отрицательным
                            //остатком от деления модуля
using namespace std;
const int N = 4;

int main()
{
  setlocale(LC_ALL, "Russian");
  wstring strAlphabet = L"АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЫЬЭЮЯ";
  int a[N];
  int e, m, phi, d = 0;
  wcout << " e = "; wcin >> e;
  wcout << " m = "; wcin >> m;
  wcout << "write numbers " << N << " symbols" << endl;
  for (int i = 0; i < N; i++){
    wcin >> a[i];
  }
  phi = getPhi(m);
  extended_euclid(e, phi, &d);
  wcout << " phi " << phi << endl;
  if (d < 0){
      d = getMod(d, phi);
  }
  wcout << " d= " << d << endl;
  for (int i = 0; i < N; i++){
    a[i] = fastExp(a[i], d, m);
    wcout << a[i] << "\t" << strAlphabet[a[i]-2] << endl;
  }
  //system("PAUSE"); //Windows crutch
  return 0;
}

