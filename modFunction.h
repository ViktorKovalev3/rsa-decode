#ifndef MODFUNCTION
#define MODFUNCTION
int getMod(int a, int const &b){
    //a mod b
    if (a < 0){
        /* Solved a problem with -12 mod 13 = 1 or -122 div 40 = 38 etc.
        Use formula: a mod b = a - (a div b) * b;
        If ABS(a) < b, then a div b = 0, but need 1 because (a div b + 1)
        When ABS(a) a mod b = -a + (a div b + 1) * b;
        */
        a = -a; //a = ABS(a);
        a = -a + (int(a / b) + 1) * b;
        return a;
    }
    else{
        return a % b;
    }
}
#endif // MODFUNCTION

